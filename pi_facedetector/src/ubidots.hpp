//
// Created by Master Móviles on 18/5/17.
//

#pragma once

#include <iostream>
#include <curl/curl.h>
#include "lib/json.hpp"
#include "lib/request.hpp"

using namespace std;
using json = nlohmann::json;

class Ubidots {
    static Ubidots *s_instance;
    string apiKeyHeader = "x-ubidots-apikey: c13514c41c3ae5735fcd3c5fbfd0a3128fc203cc";
    Ubidots() {}
public:
    bool authenticate();
    bool pushData(string device, json j);
    string getToken();
    string token;
    static Ubidots *instance()
    {
        if (!s_instance)
            s_instance = new Ubidots;
        return s_instance;
    }
};
