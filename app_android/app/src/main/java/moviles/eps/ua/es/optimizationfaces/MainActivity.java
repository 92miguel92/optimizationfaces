package moviles.eps.ua.es.optimizationfaces;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements ImagesMain.ImagesMainListener
{
    private android.support.v4.app.NotificationCompat.Builder mBuilder;
    private int contador = 1;
    private final String REQUEST = "http://178.62.95.103/api/v1/faces";
    private HttpURLConnection con = null ;
    private List<Bitmap> listImages;
    private Bitmap img64Mutable;
    private boolean first;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listImages = new ArrayList<>();
        first = true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();

        boolean firebase = intent.getBooleanExtra("firebase",false);
        InputStream is = null;
        if(firebase){
            try {
                is = new GetImage64().execute(REQUEST).get();

                getImage64FromStream(is);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }else if(first){
            is = getResources().openRawResource(getResources().getIdentifier("mock","raw", getPackageName()));
            getImage64FromStream(is);
            first=false;
        }
        Log.i("MainActivity","Token: "+FirebaseInstanceId.getInstance().getToken());
        SharedPreferences prefs = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        //sendNotification(prefs);
        if(listImages.size()>0) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fl_container, new ImagesMain(Integer.parseInt(prefs.getString("sizeImage","medium")),listImages), ImagesMain.class.getName())
                    .commit();
        }
    }

    class GetImage64 extends AsyncTask<String, Void, InputStream> {

        @Override
        protected InputStream doInBackground(String... urls) {
            HttpURLConnection con;
            InputStream is = null;

            try {
                con = (HttpURLConnection) ( new URL(urls[0])).openConnection();
                con.setRequestMethod("GET");
                con.setDoInput(true);
                con.connect();
                is = con.getInputStream();
                if(is != null){
                    updateFile(is);
                    is = getResources().openRawResource(getResources().getIdentifier("mock","raw", getPackageName()));
                }else{
                    is = getResources().openRawResource(getResources().getIdentifier("mock","raw", getPackageName()));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return(is);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.settings_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle item selection
        switch (item.getItemId()) {

            case R.id.settings:
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
                return true;
            case R.id.ubidotsFaces:
                Intent intentUbidots = new Intent(MainActivity.this, LineChartActivity.class);
                if (intentUbidots.resolveActivity(getPackageManager()) != null) {
                    startActivity(intentUbidots);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void watchFullImage() {

    }

    public void sendNotification(SharedPreferences prefs){

        mBuilder = new NotificationCompat.Builder(getApplicationContext())
                        .setTicker("Foto recibida!")
                        .setContentTitle("Numero de fotos: "+contador)
                        .setContentText("Pincha aquí para ver la foto")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setAutoCancel(false);
                        //.setContentIntent(pendingIntent);
        if(prefs.getString("vibration","NO").equals("SI")){
            mBuilder.setVibrate(new long[] { 1000, 1000 }); // Patrón de vibración en ms
        }
        mBuilder.setLights(Color.WHITE, 100, 3000); // Color y tiempos
        //Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(Uri.parse(prefs.getString("soundNotifyName","http://soundbible.com/grab.php?id=2158&type=mp3")));
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, mBuilder.build());
        contador++;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... images) {

            Bitmap imageBM = null;
            try {
                byte[] decodedString = Base64.decode(images[0], Base64.DEFAULT);
                imageBM = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return imageBM;
        }

    }

    public void getImage64FromJson(String data){

        try {
            JSONArray object = new JSONArray(data);

            for(int i=0;i<object.length();i++){
                JSONObject JSONPicture = object.getJSONObject(i);
                JSONArray images = JSONPicture.getJSONArray("data");
                img64Mutable = convertToMutable(new DownloadImageTask().execute(images.get(0).toString()).get());
                listImages.add(img64Mutable);
            }
        } catch (JSONException e) {
            System.out.println("Error: "+e.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void getImage64FromStream(InputStream is){

        try {
            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = "";
            //while ( (line = br.readLine()) != null )
                //buffer.append(line);
            line = br.readLine();
            buffer.append(line);
            is.close();
            getImage64FromJson(buffer.toString());
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }
    }

    /**
     * Converts a immutable bitmap to a mutable bitmap. This operation doesn't allocates
     * more memory that there is already allocated.
     *
     * @param imgIn - Source image. It will be released, and should not be used more
     * @return a copy of imgIn, but muttable.
     */
    public static Bitmap convertToMutable(Bitmap imgIn) {
        try {
            //this is the file going to use temporally to save the bytes.
            // This file will not be a image, it will store the raw image data.
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.tmp");

            //Open an RandomAccessFile
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");

            // get the width and height of the source bitmap.
            int width = imgIn.getWidth();
            int height = imgIn.getHeight();
            Bitmap.Config type = imgIn.getConfig();

            //Copy the byte to the file
            //Assume source bitmap loaded using options.inPreferredConfig = Config.ARGB_8888;
            FileChannel channel = randomAccessFile.getChannel();
            MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, imgIn.getRowBytes()*height);
            imgIn.copyPixelsToBuffer(map);
            //recycle the source bitmap, this will be no longer used.
            imgIn.recycle();
            System.gc();// try to force the bytes from the imgIn to be released

            //Create a new bitmap to load the bitmap again. Probably the memory will be available.
            imgIn = Bitmap.createBitmap(width, height, type);
            map.position(0);
            //load it back from temporary
            imgIn.copyPixelsFromBuffer(map);
            //close the temporary file and channel , then delete that also
            channel.close();
            randomAccessFile.close();

            // delete the temp file
            file.delete();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return imgIn;
    }

    public void updateFile(InputStream is){

        try {
            //String line = IOUtils.toString(is, "UTF-8");
            StringBuffer buffer = new StringBuffer();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = "";
            //while ( (line = br.readLine()) != null )
                //buffer.append(line);
            line = br.readLine();
            FileOutputStream fos = openFileOutput(String.valueOf(getResources().openRawResource(R.raw.mock)), MODE_PRIVATE);
            fos.write(line.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
