#include <iostream>

#import "lib/json.hpp"
#import "Ubidots.h"

using namespace std;

int main() {
    std::cout << "Autenticando..." << std::endl;
    bool auth = Ubidots::instance()->authenticate();
    if(auth) {
        std::cout << Ubidots::instance()->getToken() << std::endl;
        std::cout << "Autenticado con éxito" << std::endl;
        json j;
        j["brightness"]["value"] = 8;
        j["eyes"]["value"] = 15;
        j["faces"]["value"] = 4;
        Ubidots::instance()->pushData("raspberry", j);
    }
    else std::cout << "Error de autenticación" << std::endl;
    return 0;
}