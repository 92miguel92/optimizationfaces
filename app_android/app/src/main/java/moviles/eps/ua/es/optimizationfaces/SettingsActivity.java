package moviles.eps.ua.es.optimizationfaces;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by mastermoviles on 08/03/2017.
 */

public class SettingsActivity extends AppCompatActivity {

    private OptionsFragment fragment = null;
    private String sizeImage = "", syncTime = "", soundNotifyName = "";
    private boolean vibration;

    private void SavePreferences(String key, String value){
        SharedPreferences prefs = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key,value);
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fragment = new OptionsFragment();
        getFragmentManager().beginTransaction().replace(android.R.id.content, fragment).commit();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(fragment.sizeImage != null){
            sizeImage = OptionsFragment.sizeImage.getSummary().toString();
        }
        SavePreferences("sizeImage", sizeImage);

        if(fragment.syncTime != null){
            syncTime = OptionsFragment.syncTime.getSummary().toString();
        }
        SavePreferences("syncTime", syncTime);

        if(fragment.soundNotifyName != null){
            soundNotifyName = OptionsFragment.soundNotifyName.getSummary().toString();
        }
        SavePreferences("soundNotifyName", soundNotifyName);

        if(fragment.vibration != null){
            vibration = OptionsFragment.vibration.isChecked();
            if(vibration){
                SavePreferences("vibration", "SI");
            }
            else{
                SavePreferences("vibration", "NO");
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
