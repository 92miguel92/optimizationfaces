import express from 'express'
import Face from './models/Face'
import fsh from './fsHelper'
import firebase from 'firebase'
import FCM from 'fcm-push'
import btoa from 'btoa'

const serverKey = 'AIzaSyDIU8n7kEHXvYWQSL_ibGt8ULFijPejXqg'
const fcm = new FCM(serverKey)


export const router = express.Router()

router.get('/', (req, res) => {
  res.json({ version: 'V1.0.0' })
})

// FACES
router.get('/faces', (req, res) => {
  return Face.find().then( (faces) => {
      let parsedFaces = []
      for(var i = 0; i<faces.length; i++) {
        let parsedFace = {}
        parsedFace.rasp = faces[i].rasp
        parsedFace.date = faces[i].date
        parsedFace.data = []
        for(var e = 0; e<faces[i].image.length; e++) {
            parsedFace.data.push(btoa(faces[i].image[e]))
        }
        parsedFaces.push(parsedFace)
      }
      res.json(parsedFaces)
  })
})

router.post('/face', (req, res) => {
  let imagesArray = []
  for(let buffer of req.body.data) {
      imagesArray.push(new Buffer(buffer, "base64"))
  }
  Face.create({
    rasp: req.body.raspid || "rasp01",
    date: req.body.date || new Date('01.02.2012'),
    image: imagesArray
  }, (err, face) => {
   if (err) console.log("Error: " + err)
    else console.log("Imagen insertada", face)
  })
  res.json({ status: 'Ok' })
})


router.post('/notification', (req, res) => {
  // rasp: req.body.rasp

  var message = {
    to: 'e2eZqhSi5hw:APA91bH4FrQHl1AtTSPk2ag_lVIR-4vqqxUEBEWm1MznlwsyhVP3isfEMiJwIv0YEPGF7XqNRTH8G_a8F8wPuIesOnxi0CczZcz3ibElnS6ZIakG9p-3rMlkh6fv9QfJtURqAq3b7Xzz', // required fill with device token or topics
    priority : "high",
    data: {
      your_custom_data_key: 'your_custom_data_value'
    },
    notification : {
      body : "This week’s edition is now available.",
      title : "Prioridad alta",
      icon : "new",
    },
    data : {
      volume : "3.21.15",
      contents : "http://www.news-magazine.com/world-week/21659772"
    }
  }

  fcm.send(message)
    .then(function(response){
      console.log("Successfully sent with response: ", response);
    })
    .catch(function(err){
      console.log("Something has gone wrong!");
      console.error(err);
    })

  res.json({ response: '200' })
})
