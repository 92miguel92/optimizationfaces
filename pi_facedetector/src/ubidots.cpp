//
// Created by Master Móviles on 18/5/17.
//

#include "ubidots.hpp"
using namespace std;

// Allocate ubidots singleton
Ubidots *Ubidots::s_instance = NULL;
Request *Request::s_instance = NULL;

bool Ubidots::authenticate() {
    vector<std::string> headers = {this->apiKeyHeader};
    json body;
    json j = Request::instance()->perform("POST", "http://things.ubidots.com/api/v1.6/auth/token", headers, body);
    if (j["error"].dump().compare("null") == 0) {
        this->token = j["token"];
        return true;
    }
    else {
        cout << "ERROR: " << j["error"]<< endl;
        return false;
    }
}

bool Ubidots::pushData(string device, json data) {
    vector<std::string> headers = {
            "X-Auth-Token: " + this->token,
            "content-type: application/json"
    };
    json j = Request::instance()->perform("POST", "http://things.ubidots.com/api/v1.6/devices/" + device + "/", headers, data);
    cout << "POST: " << j << endl;
    if (j["error"].dump().compare("null") == 0) return true;
    else {
        cout << "ERROR: " << j["error"]<< endl;
        return false;
    }
}

string Ubidots::getToken() {
    return this->token;
}
