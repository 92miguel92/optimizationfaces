#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <curl/curl.h>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <sstream>
#include <time.h>
#include <iomanip>
#include <ctime>
#include "lib/base64.h"
#include "ubidots.hpp"
 
#define DEBUG 1
const std::string XML_FACE_CASCADE_NAME = "./src/classifiers/haarcascade_frontalface_alt2.xml";
const std::string XML_EYES = "./src/classifiers/haarcascade_eye_tree_eyeglasses.xml";
const std::string TITLE_WINDOW = "FaceDetector";
const std::string URL_SERVER = "178.62.95.103:80/api/v1/face";
const std::string ID_RASPBERRY = "MAC ROOM";
const double FPS = 3.0f;
const unsigned char FACE_MIN_WIDTH = 100;
const unsigned char FACE_MIN_HEIGHT = 100;
cv::CascadeClassifier classifierFaces;
cv::CascadeClassifier classifierEyes;

/**
 * Send faces detected data to server
 * @param datajson the data to send
 */
void
sendDataToServer(std::string datajson) {

    #if DEBUG
        std::cout << "data:\n" << datajson << "\n";
    #endif

    CURLcode res;
    CURL *curl = NULL;
    struct curl_slist *slist1 = NULL;
    curl = curl_easy_init();
    if(curl) {
        slist1 = curl_slist_append(slist1, "Content-Type: application/json");
        curl_easy_setopt(curl, CURLOPT_URL, URL_SERVER.c_str());
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, datajson.c_str());
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "curl/7.51.0");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist1);
        curl_easy_setopt(curl, CURLOPT_POST, 1L);
        curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 50L);
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);
        res = curl_easy_perform(curl);
        if(res == CURLE_OK) {
            std::cout << "Data sended to server correctly\n";
        }
        else {
            std::cout << "curl_easy_perform() failed: " << curl_easy_strerror(res) << "\n";
        }
        curl_easy_cleanup(curl);
        curl = NULL;
        curl_slist_free_all(slist1);
        slist1 = NULL;
    }
}

/**
 * Convert a image to base 64
 * @param frame the image 64 to convert
 * @return a base64 string
 */
std::string
imageToBase64(cv::Mat frame) {
    std::vector<uchar> buf;
    cv::imencode(".jpg", frame, buf);
    uchar *enc_msg = new uchar[buf.size()];
    for(int i=0; i < buf.size(); i++) enc_msg[i] = buf[i];
    std::string encoded = base64_encode(enc_msg, buf.size());
    return encoded;
}

/**
 * Convert a list of images in a json for send to the server
 * @param limages list of images
 * @return a json string
 */
std::string
convertToJSON(std::vector<cv::Mat> limages) {
    std::string json = "{\"raspid\":\"" + ID_RASPBERRY + "\"";
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::stringstream ss;
    ss << std::put_time(&tm, "%d/%m/%Y %H:%M:%S");
    json+=", \"date\":\"" + ss.str() + "\"";
    json+=", \"data\": [";
    for(unsigned int i=0; i<limages.size(); ++i) {
        json+="\"" + imageToBase64(limages[i]) + "\"";
        if(i<limages.size()-1) {
            json+=",";
        }
    }
    json+="] }";
    return json;
}

/**
 * Detect faces in a image
 * @param frame the image to detect faces
 * @return a list of faces
 */
std::vector<cv::Mat>
detectFaces(cv::Mat frame) {

    std::vector<cv::Rect> faces;
    std::vector<cv::Mat> faces_images;
    cv::Mat frame_gray;
    cv::cvtColor( frame, frame_gray, CV_BGR2GRAY );
    cv::equalizeHist( frame_gray, frame_gray );

    classifierFaces.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, cv::Size(30, 30) );
    faces_images.reserve(faces.size());

    for( size_t i = 0; i < faces.size(); ++i ) {
        if(faces[i].width>FACE_MIN_WIDTH && faces[i].height>FACE_MIN_HEIGHT) {
            cv::rectangle(frame, faces[i], cv::Scalar( 255, 0, 255 ), 1, 8, 0);
            faces_images.push_back(frame(faces[i]));
        }
    }
    return faces_images;
}

/**
 * Detect eyes in a frame
 * @param frame the frame to detect eyes
 * @return a vector with positions of eyes
 */
std::vector<cv::Rect>
detectEyes(cv::Mat frame) {

    std::vector<cv::Rect> rect_smiles;
    cv::Mat frame_gray;
    cv::cvtColor( frame, frame_gray, CV_BGR2GRAY );
    cv::equalizeHist( frame_gray, frame_gray );
    classifierEyes.detectMultiScale(frame_gray, rect_smiles, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, cv::Size(30, 30) );

    #if DEBUG
        for(unsigned int j=0; j<rect_smiles.size(); ++j) {
            cv::rectangle(frame, rect_smiles[j], cv::Scalar( 255, 0, 255 ), 1, 8, 0);
        }
    #endif

    return rect_smiles;
}

unsigned int
extractBightness(cv::Mat frame) {

    int count = 0;
    float brightness = 0;

    for(unsigned int i=0; i<frame.rows; i+=20) {
        for(unsigned int j=0; j<frame.cols; j+=20) {
            cv::Vec3b pixel = frame.at<cv::Vec3b>(i, j);
            brightness += (0.3*pixel[0] + 0.59*pixel[1] + 0.11*pixel[2]);
            ++count;
        }
    }
    brightness = brightness/count;
    return (unsigned int)brightness;
}

void
parseJSONForUbidots(unsigned long num_faces, unsigned long num_eyes, unsigned int brightness_level) {

    json j;
    j["brightness"]["value"] = brightness_level/16;        
    j["eyes"]["value"] = num_eyes;
    j["faces"]["value"] = num_faces;
    Ubidots::instance()->pushData("raspberry", j);
}


int main(int argc, char* argv[]) {
    
    if( !classifierFaces.load( XML_FACE_CASCADE_NAME ) ){
        std::cout << "--(!)Error loading\n"; return -1; 
    };
    
    if( !classifierEyes.load( XML_EYES ) ){
        std::cout << "--(!)Error loading\n"; return -1; 
    };

    cv::VideoCapture stream1(0);
    if (!stream1.isOpened()) { 
        std::cout << "--(!)Cannot open camera"; return -1;
    }

    //Authenticate with ubitods
    bool auth = Ubidots::instance()->authenticate();
    if(auth) {
        std::cout << Ubidots::instance()->getToken() << std::endl;
        std::cout << "Ubidots authenticate correctly" << std::endl;
    }
    else {
        std::cout << "--(!)Cannot authenticate with ubidots"; return -1;
    }


    curl_global_init(CURL_GLOBAL_ALL);


    cv::Mat frame;
    time_t start, end;
    time(&start);

    while (true) {

        time(&end);
        double seconds = difftime (end, start);
        if(seconds>=FPS) {
            stream1.read(frame);

            std::vector<cv::Mat> faces = detectFaces(frame);
            std::vector<cv::Rect> eyes = detectEyes(frame);


            if(!faces.empty() || !eyes.empty()) {
                unsigned int brightness = extractBightness(frame);
                parseJSONForUbidots(faces.size(), eyes.size(), brightness);
            }

            if(!faces.empty()) {
                std::string json = convertToJSON(faces);
        //        sendDataToServer(json);
            }
            #if DEBUG 
                cv::imshow(TITLE_WINDOW, frame);
                cv::waitKey(30);
            #endif
            time(&start);
        }    
    }
    curl_global_cleanup();
    stream1.release();
    return 0;
}