//
// Created by Master Móviles on 18/5/17.
//

#ifndef UBIDOTSC_REQUEST_H
#define UBIDOTSC_REQUEST_H

#include <iostream>
#import "json.hpp"
#import <curl/curl.h>
using json = nlohmann::json;

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

class Request {
    static Request *s_instance;
    Request() {}
public:
    json perform(std::string action, std::string url, std::vector<std::string>& headerList, json body) {
        CURL *hnd = curl_easy_init();
        std::string readBuffer;
        curl_easy_setopt(hnd, CURLOPT_CUSTOMREQUEST, action.c_str());
        curl_easy_setopt(hnd, CURLOPT_URL, url.c_str());
        curl_easy_setopt(hnd, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(hnd, CURLOPT_WRITEDATA, &readBuffer);

        struct curl_slist *headers = NULL;
        for(std::string i : headerList) {
            headers = curl_slist_append(headers, i.c_str());
        }

        std::string sBody = body.dump();
        curl_easy_setopt(hnd, CURLOPT_POSTFIELDS, sBody.c_str());

        curl_easy_setopt(hnd, CURLOPT_HTTPHEADER, headers);

        CURLcode ret = curl_easy_perform(hnd);
        if(ret == CURLE_OK) {
            auto j = json::parse(readBuffer);
            return j;
        } else {
            curl_easy_cleanup(hnd);
            return NULL;
        }
    }

    static Request *instance()
    {
        if (!s_instance)
            s_instance = new Request;
        return s_instance;
    }
};

#endif //UBIDOTSC_REQUEST_H
