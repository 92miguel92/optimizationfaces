package moviles.eps.ua.es.optimizationfaces;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

/**
 * Created by mastermoviles on 08/03/2017.
 */

public class OptionsFragment extends PreferenceFragment{

    public static ListPreference sizeImage, syncTime, soundNotifyName;
    public static SwitchPreference vibration;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.options);

        sizeImage = (ListPreference) findPreference("sizeImage");
        if(sizeImage.getValue() == null){
            sizeImage.setValueIndex(0);
        }
        sizeImage.setSummary(sizeImage.getValue().toString());

        syncTime = (ListPreference) findPreference("syncTime");
        if(syncTime.getValue() == null){
            syncTime.setValueIndex(0);
        }
        syncTime.setSummary(syncTime.getValue().toString());

        soundNotifyName = (ListPreference) findPreference("soundNotifyName");
        if(soundNotifyName.getValue() == null){
            soundNotifyName.setValueIndex(0);
        }
        soundNotifyName.setSummary(soundNotifyName.getValue().toString());

        vibration = (SwitchPreference) findPreference("vibration");
    }

    @Override
    public void onResume() {
        super.onResume();

        sizeImage.setOnPreferenceChangeListener(new EditTextPreference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(newValue.toString());
                return true;
            }
        });

        syncTime.setOnPreferenceChangeListener(new EditTextPreference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(newValue.toString());
                return true;
            }
        });

        soundNotifyName.setOnPreferenceChangeListener(new EditTextPreference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(newValue.toString());
                return true;
            }
        });

        vibration.setOnPreferenceChangeListener(new EditTextPreference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(newValue.toString());
                return true;
            }
        });
    }
}
