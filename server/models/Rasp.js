import mongoose from 'mongoose'
import findOrCreate from 'mongoose-findorcreate'

const Schema = mongoose.Schema

let RashSchema = new Schema({
  serial: String,
  faces: [{
    type: Schema.Types.ObjectId,
    ref: 'Face'
  }]
})

RashSchema.plugin(findOrCreate)

export default mongoose.model('Rasp', RashSchema)