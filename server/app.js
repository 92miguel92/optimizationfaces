import express from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'

import {router} from './routes'
const app = express()

//Body parser config
app.use(bodyParser.urlencoded({extend: true, limit: '16mb'}))
app.use(bodyParser.json())

app.use('/api/v1', router)

app.listen(3000, () => {
  console.log("Express server running on port 3000")

  mongoose.Promise = Promise;
  mongoose.connect('mongodb://localhost/LFD').then(() => {
    console.log("Connected to MongoDB")
  })
})
