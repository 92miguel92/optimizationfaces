import mongoose from 'mongoose'

const Schema = mongoose.Schema
const date = new Date()

let FaceSchema = new Schema({
  rasp: String,
  date: Date,
  image: [Buffer]
})

export default mongoose.model('Face', FaceSchema)
