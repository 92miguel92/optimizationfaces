package moviles.eps.ua.es.optimizationfaces;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mastermoviles on 08/03/2017.
 */

public class ImagesMain extends Fragment {

    private ImagesMainListener listener;
    private ImageButton image;
    private List<Bitmap> imagesList= new ArrayList<>();
    private int sizeImage,total;
    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    public interface ImagesMainListener{
        void watchFullImage();
    }

    public ImagesMain(){}

    public ImagesMain(int sizeImage, List<Bitmap> imagesList){
        this.sizeImage=sizeImage;
        this.imagesList = imagesList;
        this.total=imagesList.size();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (ImagesMainListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnItemSelectedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        LinearLayout gameBoard = (LinearLayout) view.findViewById(R.id.linearImages);
        int totalAux = total;
        if(total < 4){
            totalAux = 4;
        }
        ImageView[] btnGreen = new ImageView[totalAux];
        int lines = 0, heightLayout = 200;
        switch(sizeImage){
            case 2: lines = (total<=4) ? 1 : (total%4==0) ? total/4 : total/4+1;
                heightLayout = 150;
                break;
            case 4: lines = (total<=2) ? 1 : (total%2==0) ? total/2 : total/2+1;
                heightLayout = 300;
                break;
            case 8: lines = total;
                heightLayout = 600;
                break;
        }
        int contador = 0;
        for(int i=0; i<lines;i++){
            LinearLayout gameBoardson = new LinearLayout(getActivity());
            gameBoardson.setOrientation(LinearLayout.HORIZONTAL);
            gameBoardson.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, heightLayout));
            gameBoardson.setWeightSum(8.0f);
            for(int j=0; j<(gameBoardson.getWeightSum()/sizeImage) && contador < total;j++){
                btnGreen[j] = new ImageView(getActivity());
                btnGreen[j].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,(float)sizeImage));
                btnGreen[j].setScaleX(2);
                btnGreen[j].setScaleY(1);
                btnGreen[j].setImageBitmap(imagesList.get(contador));
                btnGreen[j].setTag(j);
                btnGreen[j].setId(j);
                gameBoardson.addView(btnGreen[j]);
                if(contador == total-1){
                    int k = j+1;
                    while(k<gameBoardson.getWeightSum()/sizeImage){
                        btnGreen[k] = new ImageView(getActivity());
                        btnGreen[k].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,(float)sizeImage));
                        btnGreen[k].setBackgroundColor(Color.TRANSPARENT);
                        gameBoardson.addView(btnGreen[k]);
                        k++;
                    }
                }
                contador++;
            }
            gameBoard.addView(gameBoardson);
        }

        return view;
    }
}
